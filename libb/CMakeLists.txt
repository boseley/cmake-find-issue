cmake_minimum_required(VERSION 3.0)

project(libb)

find_library(liba REQUIRED)
add_library(${PROJECT_NAME} INTERFACE )

target_include_directories(${PROJECT_NAME} INTERFACE
        "$<INSTALL_INTERFACE:include>"
        "$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>")

install(DIRECTORY include/${PROJECT_NAME} 
        DESTINATION include)

install(TARGETS ${PROJECT_NAME}
        EXPORT ${PROJECT_NAME}Config
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib
        RUNTIME DESTINATION bin
        INCLUDES DESTINATION include
)

install(EXPORT ${PROJECT_NAME}Config
        FILE ${PROJECT_NAME}Config.cmake
        DESTINATION lib/cmake/${PROJECT_NAME}
)        

