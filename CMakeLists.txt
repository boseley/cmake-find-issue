cmake_minimum_required(VERSION 3.0)
project(cmake_find_package_issue)
include(ExternalProject)

set(DEV_ROOT ${CMAKE_BINARY_DIR}/devroot)

set(CMAKE_ARGS
        -DCMAKE_STAGING_PREFIX=${DEV_ROOT}
        -DCMAKE_PREFIX_PATH=${DEV_ROOT}
    )

ExternalProject_add(
        liba
        CMAKE_ARGS ${CMAKE_ARGS}
        SOURCE_DIR ${CMAKE_SOURCE_DIR}/liba
)

ExternalProject_add(
        libb
        DEPENDS liba
        CMAKE_ARGS ${CMAKE_ARGS}
        SOURCE_DIR ${CMAKE_SOURCE_DIR}/libb
)